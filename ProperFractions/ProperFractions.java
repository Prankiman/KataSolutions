public class ProperFractions {
    public static long properFractions(long n) {
    
      /**
      * computes the euler totient of n
      * e.g properFractions(6) = 6 * (1-1/2) * (1 – 1/3) = 2.
      */
    
      if (n == 1)
        return 0;
      
      long result = n;

      // check for prime factors of n and compute the toitent 
      // of n using *eulers product formula* 
      // (https://en.wikipedia.org/wiki/Euler%27s_totient_function)
      for (long p = 2; p * p <= n; p++) {
          if (n % p == 0) {               
            while(n % p == 0)
              n /= p;
            result *= (1.0 - (1.0 / p));
          }
      }

    if (n > 1)
      result -= result/n;
    
    return result;
    
  }
}
