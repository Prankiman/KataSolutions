#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char *formatDuration (int n) {
  int years = n/31536000;
  n -= years * 31536000;
  int days = n/86400;
  n -= days * 86400;
  int hours = n/3600;
  n -= hours * 3600;
  int minutes = n/60;
  n -= minutes * 60;
  
  char * human_readable_time_format = (char*)malloc(200 * sizeof(char));

  if(years){
    years == 1 ? sprintf(human_readable_time_format, "1 year") : sprintf(human_readable_time_format, "%d years", years);
    int other_time_metrics[4] = {days, hours, minutes, n};
    int num_other_time_metrics = 0;
    for (int i = 0; i < 4; i++)
      if (other_time_metrics[i])
        num_other_time_metrics ++;
    printf("%d\n", num_other_time_metrics);
    num_other_time_metrics == 0 ? 0 : num_other_time_metrics == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), " and ") : sprintf(human_readable_time_format + strlen(human_readable_time_format), ", ");
  }
  if(days){
    days == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), "1 day") : sprintf(human_readable_time_format + strlen(human_readable_time_format), "%d days", days);
    int other_time_metrics[3] = {hours, minutes, n};
    int num_other_time_metrics = 0;
    for (int i = 0; i < 3; i++)
      if (other_time_metrics[i])
        num_other_time_metrics ++;
    
    printf("%d\n", num_other_time_metrics);
    num_other_time_metrics == 0 ? 0 : num_other_time_metrics == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), " and ") : sprintf(human_readable_time_format + strlen(human_readable_time_format), ", ");
  }
  if(hours){
    hours == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), "1 hour") : sprintf(human_readable_time_format + strlen(human_readable_time_format), "%d hours", hours);
    int other_time_metrics[2] = {minutes, n};
    int num_other_time_metrics = 0;
    for (int i = 0; i < 2; i++)
      if (other_time_metrics[i])
        num_other_time_metrics ++;
    
    printf("%d\n", num_other_time_metrics);
    num_other_time_metrics == 0 ? 0 : num_other_time_metrics == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), " and ") : sprintf(human_readable_time_format + strlen(human_readable_time_format), ", ");
  }
  if(minutes){
    minutes == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), "1 minute") : sprintf(human_readable_time_format + strlen(human_readable_time_format), "%d minutes", minutes);
    
    n > 0 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), " and ") : 0;
  }
  if (n > 0){
    n == 1 ? sprintf(human_readable_time_format + strlen(human_readable_time_format), "1 second") : sprintf(human_readable_time_format + strlen(human_readable_time_format), "%d seconds", n);
  }
  
  if(n == 0 && minutes == 0 && hours == 0 && years == 0){
    sprintf(human_readable_time_format, "now");
  }
 
  printf("%s\n", human_readable_time_format);
  printf("%d years, %d days, %d hours, %d minutes, %d seconds", years, days, hours, minutes, n);
    
  return human_readable_time_format;
}
