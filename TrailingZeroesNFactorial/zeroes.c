long zeros(long n) {
  /* returns the amount of trailing zeroes in the factorial of "n"
     e.g. zeroes(5) = 1, zeroes(6) = !, zeroes(10) = 2, zeroes(15) = 3, zeroes(25) = 6, zeroes (30) = 7
  */
  
  // trailing zeroes = how many times 5 divides n + times 5^2 divides n ... + 5^p divides n
  return (n / 5) + (n / 25) + (n / 125) + (n / 625) + (n / 3125) + (n / 15625) + (n / 78125) + 
  (n / 390625) + (n / 1953125) + (n / 9765625) + (n / 48828125) + (n/ 244140625) + (n / 1220703125);
}
